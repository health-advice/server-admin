<?php

use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Tài khoản người sử dụng
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'user',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'UserController@index');
        $router->post('create', 'UserController@create');
        $router->get('show/{id}', 'UserController@show');
        $router->post('update/{id}', 'UserController@update');
        $router->delete('remove/{id}', 'UserController@remove');
        $router->post('searchAll', 'UserController@searchAll');
//    });
});

// Product
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'product',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'ProductController@index');
        $router->post('create', 'ProductController@create');
        $router->get('show/{id}', 'ProductController@show');
        $router->post('update/{id}', 'ProductController@update');
        $router->delete('remove/{id}', 'ProductController@remove');
        $router->post('searchAll', 'ProductController@searchAll');
        $router->post('upload', 'ProductController@upload');
//    });
});

// Exercise
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'exercise',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
    $router->get('list', 'ExerciseController@index');
    $router->post('create', 'ExerciseController@create');
    $router->get('show/{id}', 'ExerciseController@show');
    $router->post('update/{id}', 'ExerciseController@update');
    $router->delete('remove/{id}', 'ExerciseController@remove');
    $router->post('searchAll', 'ExerciseController@searchAll');
    $router->post('upload', 'ExerciseController@upload');
//    });
});

// Order
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'order',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
    $router->get('list', 'OrderController@index');
    $router->delete('remove/{id}', 'OrderController@remove');
    $router->post('searchAll', 'OrderController@searchAll');
    $router->post('checkOrder/{id}', 'OrderController@checkOrder');
//    });
});

// CheckedOrder
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'checkedOrder',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
    $router->get('list', 'CheckedOrderController@index');
    $router->delete('remove/{id}', 'CheckedOrderController@remove');
    $router->post('searchAll', 'CheckedOrderController@searchAll');
//    });
});

// Comment
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'comment',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
    $router->get('list', 'CommentController@index');
    $router->delete('remove/{id}', 'CommentController@remove');
    $router->post('searchAll', 'CommentController@searchAll');
//    });
});

// NEWS
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'news',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'NewsController@index');
        $router->post('create', 'NewsController@create');
        $router->get('show/{id}', 'NewsController@show');
        $router->post('update/{id}', 'NewsController@update');
        $router->delete('remove/{id}', 'NewsController@remove');
        $router->post('searchAll', 'NewsController@searchAll');
        $router->post('upload', 'NewsController@upload');
//    });
});
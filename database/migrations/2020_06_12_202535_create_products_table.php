<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            // Thông tin cơ bản
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('name', 200)->comment('Tên sản phẩm');
            $table->decimal('price')->default(0)->comment('Đơn giá');
            $table->text('description')->comment('Mô tả thêm');

            // Hình ảnh, video và vị trí
            $table->string('thumbnails')->default('')->comment('Hình đại diện');

            $table->unsignedInteger('product_type')->comment('Loại sản phẩm');
            $table->bigInteger('read_count')->comment('Số lượt xem');
            $table->bigInteger('calories')->comment('Calories');

            $table->date('start_date')->nullable()->comment('Ngày bắt đầu');
            $table->date('end_date')->nullable()->comment('Ngày kết thúc');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

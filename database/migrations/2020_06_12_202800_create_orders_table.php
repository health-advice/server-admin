<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_id')->comment('Mã sản phẩm, khóa ngoại');
            $table->unsignedBigInteger('user_id')->comment('Mã user, khóa ngoại');

            $table->text('contact_name')->comment('Họ tên');
            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');
            $table->text('description')->comment('Mô tả thêm');
            $table->text('contact_mobile')->comment('Số điện thoại');
            $table->text('contact_address')->comment('Địa chỉ giao hàng');
            $table->integer('product_count')->comment('Số lượng');
            $table->decimal('total_price')->comment('Tổng giá');
            $table->date('order_date')->comment('Ngày đặt hàng');
            $table->date('receive_date')->comment('Ngày giao hàng');
            $table->tinyInteger('status')->default(0)->comment('Trạng thái đơn hàng 0 = đang xử lý, 1 = đã duyệt');

            $table->index(['product_id']);
            $table->index(['user_id']);

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

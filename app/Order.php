<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'product_id',
        'user_id',
        'description',
        'product_count',
        'total_price',
        'order_date',
        'receive_date',
        'contact_name',
        'sex',
        'contact_mobile',
        'contact_address',
        'status',
    ];

    protected $filter = [
        'id',
        'product_id',
        'user_id',
        'description',
        'product_count',
        'total_price',
        'order_date',
        'receive_date',
        'contact_name',
        'sex',
        'contact_mobile',
        'contact_address',
        'status',
    ];

    public function products()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}

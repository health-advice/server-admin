<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\ExerciseCreateRequest;
use App\Exercise;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExerciseController extends AbstractApiController
{
    public function index(Request $request)
    {
        $exercise = Exercise::query()
            ->select([
                'id',
                'title',
                'slug',
                'content',
                'content_type',
                'thumbnails',
                'source',
                'start_date',
                'end_date',
                'read_count',
            ])
            ->DataTablePaginate($request);

        return $this->item($exercise);
    }

    public function create(ExerciseCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['title']                           = $validatedData['title'];
        $payload['slug']                            = $slugify->slugify($validatedData['title']);
        $payload['content']                         = $validatedData['content'];
        $payload['content_type']                    = $validatedData['content_type'];
        $payload['thumbnails']                      = $validatedData['thumbnails'];
        $payload['source']                          = $validatedData['source'];
        $payload['start_date']                      = $validatedData['start_date'];
        $payload['end_date']                        = $validatedData['end_date'];
        $payload['read_count']                      = 0;

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $exercise = Exercise::create($payload);
        DB::beginTransaction();

        try {
            $exercise->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm bài tập thành công!');
            $this->setStatusCode(200);
            $this->setData($exercise);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Exercise::query()->findOrFail($id);
    }

    public function update(ExerciseCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $exercise = Exercise::query()->findOrFail($id);
        if (!$exercise) {
            $this->setMessage('Không có bài tập này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $exercise->title                            = $validatedData['title'];
                $exercise->slug                             = $slugify->slugify($validatedData['title']);
                $exercise->content                          = $validatedData['content'];
                $exercise->content_type                     = $validatedData['content_type'];
                $exercise->thumbnails                       = $validatedData['thumbnails'];
                $exercise->source                           = $validatedData['source'];
                $exercise->start_date                       = $validatedData['start_date'];
                $exercise->end_date                         = $validatedData['end_date'];
                $exercise->read_count                       = $validatedData['read_count'];

                $exercise->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($exercise);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Exercise::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($title)
    {
        $exercise = Exercise::query()->get();
        foreach ($exercise->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $exercise = Exercise::query()
            ->select([
                'id',
                'title',
                'slug',
                'content',
                'content_type',
                'thumbnails',
                'source',
                'start_date',
                'end_date',
                'read_count',
            ])
            ->where('title', 'LIKE', "%$search%")
            ->orWhere('source', 'LIKE', "%$search%")
            ->orWhere('content_type', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($exercise);
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/exercise'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}

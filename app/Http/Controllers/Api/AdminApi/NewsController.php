<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\NewsCreateRequest;
use App\News;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewsController extends AbstractApiController
{
    public function index(Request $request)
    {
        $news = News::query()
            ->select([
                'id',
                'title',
                'slug',
                'content',
                'content_type',
                'thumbnails',
                'source',
                'start_date',
                'end_date',
                'read_count',
            ])
            ->DataTablePaginate($request);

        return $this->item($news);
    }

    public function create(NewsCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['title']                           = $validatedData['title'];
        $payload['slug']                            = $slugify->slugify($validatedData['title']);
        $payload['content']                         = $validatedData['content'];
        $payload['content_type']                    = ! empty($validatedData['content_type']) ? $validatedData['content_type'] : 'html';
        $payload['thumbnails']                      = $validatedData['thumbnails'];
        $payload['source']                          = $validatedData['source'];
        $payload['start_date']                      = $validatedData['start_date'];
        $payload['end_date']                        = $validatedData['end_date'];
        $payload['read_count']                      = 0;

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $news = News::create($payload);
        DB::beginTransaction();

        try {
            $news->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm tin tức thành công!');
            $this->setStatusCode(200);
            $this->setData($news);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return News::query()->findOrFail($id);
    }

    public function update(NewsCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $news = news::query()->findOrFail($id);
        if (!$news) {
            $this->setMessage('Không có tin tức này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $news->title                            = $validatedData['title'];
                $news->slug                             = $slugify->slugify($validatedData['title']);
                $news->content                          = $validatedData['content'];
                $news->content_type                     = $validatedData['content_type'];
                $news->thumbnails                       = $validatedData['thumbnails'];
                $news->source                           = $validatedData['source'];
                $news->start_date                       = $validatedData['start_date'];
                $news->end_date                         = $validatedData['end_date'];
                $news->read_count                       = $validatedData['read_count'];

                $news->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($news);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        News::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($title)
    {
        $news = News::query()->get();
        foreach ($news->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $news = News::query()
            ->select([
                'id',
                'title',
                'slug',
                'content',
                'content_type',
                'thumbnails',
                'source',
                'start_date',
                'end_date',
                'read_count',
            ])
            ->where('title', 'LIKE', "%$search%")
            ->orWhere('content', 'LIKE', "%$search%")
            ->orWhere('content_type', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($news);
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/news'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}

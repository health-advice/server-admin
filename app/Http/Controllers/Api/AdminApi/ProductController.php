<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\ProductCreateRequest;
use App\Product;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends AbstractApiController
{
    public function index(Request $request)
    {
        $product = Product::query()
            ->select([
                'id',
                'name',
                'slug',
                'price',
                'description',
                'thumbnails',
                'product_type',
                'start_date',
                'end_date',
                'read_count',
                'calories',
            ])
            ->DataTablePaginate($request);

        return $this->item($product);
    }

    public function create(ProductCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['name']                            = $validatedData['name'];
        $payload['slug']                            = $slugify->slugify($validatedData['name']);
        $payload['price']                           = $validatedData['price'];
        $payload['description']                     = $validatedData['description'];
        $payload['thumbnails']                      = $validatedData['thumbnails'];
        $payload['product_type']                    = $validatedData['product_type'];
        $payload['start_date']                      = $validatedData['start_date'];
        $payload['end_date']                        = $validatedData['end_date'];
        $payload['read_count']                      = 0;
        $payload['calories']                        = $validatedData['calories'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên sản phẩm');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $product = Product::create($payload);
        DB::beginTransaction();

        try {
            $product->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm sản phẩm thành công!');
            $this->setStatusCode(200);
            $this->setData($product);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Product::query()->findOrFail($id);
    }

    public function update(ProductCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $product = Product::query()->findOrFail($id);
        if (!$product) {
            $this->setMessage('Không có sản phẩm này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $product->name                             = $validatedData['name'];
                $product->slug                             = $slugify->slugify($validatedData['name']);
                $product->price                            = $validatedData['price'];
                $product->description                      = $validatedData['description'];
                $product->thumbnails                       = $validatedData['thumbnails'];
                $product->product_type                     = $validatedData['product_type'];
                $product->start_date                       = $validatedData['start_date'];
                $product->end_date                         = $validatedData['end_date'];
                $product->read_count                       = $validatedData['read_count'];
                $product->calories                         = $validatedData['calories'];

                $product->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($product);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Product::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $product = Product::query()->get();
        foreach ($product->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $product = Product::query()
            ->select([
                'id',
                'name',
                'slug',
                'price',
                'description',
                'thumbnails',
                'product_type',
                'start_date',
                'end_date',
                'read_count',
                'calories',
            ])
            ->where('name', 'LIKE', "%$search%")
            ->orWhere('price', 'LIKE', "%$search%")
            ->orWhere('product_type', 'LIKE', "%$search%")
            ->orWhere('calories', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($product);
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/product'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}

<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Order;
use App\Http\Controllers\AbstractApiController;
use Illuminate\Http\Request;

class CheckedOrderController extends AbstractApiController
{
    public function index(Request $request)
    {
        $order = Order::query()
            ->select([
                'id',
                'product_id',
                'user_id',
                'description',
                'product_count',
                'total_price',
                'order_date',
                'receive_date',
                'contact_name',
                'sex',
                'contact_mobile',
                'contact_address',
                'status'
            ])
            ->with('products', 'users')
            ->where('status', '=', 1)
            ->DataTablePaginate($request);

        return $this->item($order);
    }


    public function remove($id)
    {
        Order::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $order = Order::query()
            ->select([
                'id',
                'product_id',
                'user_id',
                'description',
                'product_count',
                'total_price',
                'order_date',
                'receive_date',
                'contact_name',
                'sex',
                'contact_mobile',
                'contact_address',
                'status',
            ])
            ->with('products', 'users')
            ->where('status', '=', 1)
            ->orWhere('product_id', 'LIKE', "%$search%")
            ->orWhere('user_id', 'LIKE', "%$search%")
            ->orWhere('product_count', 'LIKE', "%$search%")
            ->orWhere('total_price', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($order);
    }
}

<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Requests\ProductCreateRequest;
use App\Order;
use App\Http\Controllers\AbstractApiController;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends AbstractApiController
{
    public function index(Request $request)
    {
        $order = Order::query()
            ->select([
                'id',
                'product_id',
                'user_id',
                'description',
                'product_count',
                'total_price',
                'order_date',
                'receive_date',
                'contact_name',
                'sex',
                'contact_mobile',
                'contact_address',
                'status'
            ])
            ->with('products', 'users')
            ->where('status', '=', 0)
            ->DataTablePaginate($request);

        return $this->item($order);
    }

    public function checkOrder($id)
    {
        $order = Order::query()->findOrFail($id);
        if (!$order) {
            $this->setMessage('Không có sản phẩm này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $order->status                            = 1;

                $order->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($order);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }


    public function remove($id)
    {
        Order::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $order = Order::query()
            ->select([
                'id',
                'product_id',
                'user_id',
                'description',
                'product_count',
                'total_price',
                'order_date',
                'receive_date',
                'contact_name',
                'sex',
                'contact_mobile',
                'contact_address',
                'status',
            ])
            ->where('product_id', 'LIKE', "%$search%")
            ->orWhere('user_id', 'LIKE', "%$search%")
            ->orWhere('product_count', 'LIKE', "%$search%")
            ->orWhere('total_price', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($order);
    }
}

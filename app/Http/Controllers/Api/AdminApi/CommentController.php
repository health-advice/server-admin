<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;
use App\Comment;
use Illuminate\Http\Request;

class CommentController extends AbstractApiController
{
    public function index(Request $request)
    {
        $comment = Comment::query()
            ->select([
                'id',
                'product_id',
                'user_id',
                'content',
                'comment_date',
            ])
            ->with('products', 'users')
            ->DataTablePaginate($request);

        return $this->item($comment);
    }


    public function remove($id)
    {
        Comment::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $comment = Comment::query()
            ->select([
                'id',
                'product_id',
                'user_id',
                'content',
                'comment_date',
            ])
            ->with('products', 'users')
            ->where('product_id', 'LIKE', "%$search%")
            ->orWhere('user_id', 'LIKE', "%$search%")
            ->orWhere('content', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($comment);
    }
}

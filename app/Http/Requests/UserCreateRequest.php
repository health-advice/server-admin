<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'                                      => 'required',
            'password'                                      => 'required',
            'email'                                         => 'required',
            'mobile'                                        => 'required',
            'last_name'                                     => 'required',
            'first_name'                                    => 'required',
            'sex'                                           => 'required',
            'address'                                       => 'required',
            'role'                                          => 'nullable',
            'status'                                        => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'username.required'                             => 'Bạn chưa nhập tài khoản',
            'password.required'                             => 'Bạn chưa nhập mật khẩu',
            'email.required'                                => 'Bạn chưa nhập email',
            'mobile.required'                               => 'Bạn chưa nhập số điện thoại di động',
            'last_name.required'                            => 'Bạn chưa nhập họ',
            'first_name.required'                           => 'Bạn chưa nhập tên',
            'sex.required'                                  => 'Bạn chưa nhập giới tính',
            'address.required'                              => 'Bạn chưa nhập địa chỉ liên hệ',
        ];
    }
}

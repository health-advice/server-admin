<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                                  => 'required',
            'price'                                 => 'required',
            'description'                           => 'required',
            'thumbnails'                            => 'required',
            'product_type'                          => 'required',
            'start_date'                            => 'required',
            'end_date'                              => 'required',
            'calories'                              => 'required',
            'read_count'                            => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'name.required'                         => 'Bạn chưa nhập tên sản phẩm',
            'price.required'                        => 'Bạn chưa nhập đơn giá',
            'description.required'                  => 'Bạn chưa nhập nội dung',
            'thumbnails.required'                   => 'Bạn chưa nhập hình ảnh',
            'product_type.required'                 => 'Bạn chưa nhập loại sản phẩm',
            'start_date.required'                   => 'Bạn chưa nhập ngày bắt đầu đăng tin',
            'end_date.required'                     => 'Bạn chưa nhập ngày kết thúc đăng tin',
            'calories.required'                     => 'Bạn chưa nhập calo',
        ];
    }
}

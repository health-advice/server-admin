<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'product_id',
        'user_id',
        'content',
        'comment_date',
    ];

    protected $filter = [
        'id',
        'product_id',
        'user_id',
        'content',
        'comment_date',
    ];

    public function products()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}

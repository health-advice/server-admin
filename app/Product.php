<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'name',
        'slug',
        'price',
        'description',
        'thumbnails',
        'product_type',
        'start_date',
        'end_date',
        'read_count',
        'calories',
    ];

    protected $filter = [
        'id',
        'name',
        'slug',
        'price',
        'description',
        'thumbnails',
        'product_type',
        'start_date',
        'end_date',
        'read_count',
        'calories',
    ];
}
